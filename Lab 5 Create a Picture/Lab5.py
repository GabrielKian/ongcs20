# Gabriel Ong
# CS 20
# Mr. Birrell
# March 11, 2020
# Lab 5 Make a Pretty Picture
import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
DARK_RED = (128, 0, 0)
BABY_BLUE = (85, 255, 255)
YELLOW = (255, 255, 0)
GREY = (170, 170, 170)
LIGHT_GREY = (230, 230, 230)

pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Belli Dura Despicio")

# Loop until the user clicks the close button.
done = False

# Screen update
clock = pygame.time.Clock()

# Main Program
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here (kek)

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(WHITE)

    # --- Drawing code
    font = pygame.font.SysFont('Times New Roman', 25, False, True)

    # Sword Art
    # Tip/Point
    pygame.draw.polygon(screen, YELLOW, [[45, 360], [200, 275], [310, 300], [230, 360], [310, 420], [210, 445]])
    pygame.draw.polygon(screen, RED, [[50, 360], [200, 280], [300, 300], [230, 360], [300, 420], [210, 440]])

    # Central Ridge (gold)
    pygame.draw.polygon(screen, YELLOW, [[300, 325], [320, 315], [460, 300], [500, 330],
                                         [500, 390], [460, 420], [320, 405], [300, 395]])

    # Fuller (gold)
    pygame.draw.rect(screen, YELLOW, [230, 325, 590, 70])

    # Central Ridge (red)
    pygame.draw.polygon(screen, RED, [[300, 335], [320, 325], [460, 310], [500, 340],
                                      [500, 380], [460, 410], [320, 395], [300, 385]])

    # Tip (gold)
    pygame.draw.polygon(screen, YELLOW, [[130, 360], [230, 295], [280, 325], [280, 395], [230, 425]])

    # Fuller (red)
    pygame.draw.rect(screen, RED, [230, 330, 600, 60])

    # Central Ridge (dark_red)
    pygame.draw.polygon(screen, DARK_RED, [[420, 345], [460, 320], [490, 345], [490, 375], [460, 400], [420, 375]])

    # Point (red)
    pygame.draw.polygon(screen, RED, [[150, 360], [230, 312], [310, 360], [230, 408]])
    pygame.draw.polygon(screen, DARK_RED, [[200, 335], [230, 325], [265, 345], [200, 345],
                                           [200, 385], [230, 395], [265, 375], [200, 375]])

    # Fuller (grey)
    pygame.draw.rect(screen, GREY, [230, 345, 600, 30])

    # Point (grey)
    pygame.draw.polygon(screen, GREY, [[230, 345], [200, 330], [150, 360], [200, 390], [230, 375]])

    # Rain Guard
    pygame.draw.polygon(screen, DARK_RED, [[780, 360], [820, 325], [880, 340], [900, 325], [950, 340], [1000, 325],
                                           [1000, 395], [950, 380], [900, 395], [880, 380], [820, 395]])
    pygame.draw.polygon(screen, RED, [[785, 360], [820, 332], [880, 345], [900, 330], [950, 345], [980, 330],
                                      [980, 390], [950, 375], [900, 390], [880, 375], [820, 390]])

    # Buckle
    pygame.draw.rect(screen, YELLOW, [850, 350, 40, 20], 5)

    # Hand Guard
    pygame.draw.polygon(screen, YELLOW, [[890, 350], [970, 345], [980, 330], [1000, 320], [1000, 310], [920, 310],
                                         [900, 325], [860, 305], [900, 285], [1000, 285], [1010, 275], [1020, 275],
                                         [1020, 285], [1030, 295], [1030, 325], [1020, 340], [1040, 345], [1040, 375],
                                         [1020, 375], [1030, 400], [1030, 425], [1030, 425], [1020, 435], [1020, 445],
                                         [1010, 445], [1000, 435], [900, 435], [860, 415], [900, 395], [920, 410],
                                         [1000, 410], [1000, 400], [980, 390], [970, 375], [890, 370]])

    # Hilt
    # Handle
    pygame.draw.rect(screen, LIGHT_GREY, [1040, 345, 130, 30])
    x_offset = 0
    while x_offset < 120:
        pygame.draw.line(screen, GREY, [1045 + x_offset, 345], [1060 + x_offset, 375], 5)
        x_offset = x_offset + 20

    # Pommel
    pygame.draw.ellipse(screen, YELLOW, [1160, 335, 40, 50])
    pygame.draw.ellipse(screen, BABY_BLUE, [980, 345, 50, 30])

    # Inscription On Blade
    text = font.render("Belli Dura Despicio", True, BLACK)
    screen.blit(text, [480, 345])

    # --- Updates the Screen
    pygame.display.flip()

    # --- 60 Frames Per Second
    clock.tick(60)

# Quit
pygame.quit()
