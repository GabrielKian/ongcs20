# calculator to find area of a trapezoid

print("this calculator is used to find the area of trapezoids")

# input

height_of_a_trapezoid = float(input("Enter the height of the trapezoid:"))
length_of_the_bottom_base = float(input("Enter the length of the bottom base:"))
length_of_the_top_base = float(input("Enter the length of the top base:"))
area = ((length_of_the_top_base + length_of_the_bottom_base) / 2) * height_of_a_trapezoid

# output

print("area:", area)
