# calculator for converting Fahrenheit into Celsius

print("This calculator is used to convert Fahrenheit into Celsius")

# input

fahrenheit = float(input("Enter Fahrenheit here:"))
celsius = (fahrenheit - 32) * (5/9)

# output

print("celsius:", celsius)
