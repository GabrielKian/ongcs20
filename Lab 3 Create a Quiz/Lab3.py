# Gabriel Ong
# CS 10
# Mr. Birrell
# February 14, 2020

name = input("What is your name: ")
score = 0
points = 1
if name.lower() == "gabriel" or name.lower() == "ong" or name.lower() == "aoki":
    print("Welcome back Aoki-kun.")
else:
    print("Good day,", name)

print("There is still much work to do. Help me by solving these questions:\n\n")

# quiz start
# Question 1 menu answer

q_one = input("Who gets blown out of the airlock in the movie, 'Interstellar'?\nA: Dr. Mann\nB: Cooper\nC: Dr. "
              "Brandt\nD: TARS\n\nType your answer here:")
if q_one.lower() == "a" or q_one.lower() == "dr.mann" or q_one.lower() == "dr. mann" or q_one.lower() == "mann" or \
        q_one.lower() == "doctor mann":
    score += points
    print("\nCorrect! Your score is:", score, "/5\n\n")
else:
    print("\nIncorrect! The correct answer is: 'A: Dr. Mann'\nYour score is:", score, "/5\n\n")

# question 2 number answer

q_two = input("If I walk 3 kilometers North then 4 kilometers west, how many kilometers am I from my starting point? "
              "\n\nType your answer here:")
if q_two == "5" or q_two == "5km" or q_two.lower == "five" or q_two.lower() == "five kilometers":
    score += points
    print("\nCorrect! Your score is:", score, "/5\n\n")
else:
    print("\nIncorrect! The correct answer is: '5km'\nYour score is:", score, "/5\n\n")

# question 3 number menu answer

q_three = input("How many World Wars have there been so far?\n1\n2\n3\n4\n\nType your answer here:")
if q_three.lower() == "two" or q_three == "2":
    score += points
    print("\nCorrect! Your score is:", score, "/5\n\n")
else:
    print("\nIncorrect! The correct answer is: 2\nYour score is:", score, "/5\n\n")

# question 4 text answer

q_four = input("What nation is well known for having the best maple syrup in the world?\n\nType your answer here:")
if q_four.lower() == "canada":
    score += points
    print("\nCorrect! Your score is:", score, "/5\n\n")
else:
    print("\nIncorrect! The correct answer is: 'Canada'\nYour score is:", score, "/5\n\n")

# question 5
q_five = input("What is the square root of'64'?\nType your answer here:")
if q_five == "8" or q_five.lower() == "eight":
    score += points
    print("\nCorrect! Your score is:", score, "/5\n\n")
else:
    print("\nIncorrect! The correct answer is: '8'\nYour score is:", score, "/5\n\n")

# quiz end
print("Congratulations! You finished the quiz.\nYour final score is:", (score / 5) * 100, "%")
