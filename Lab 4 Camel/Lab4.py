# Gabriel Ong
# Liam Ledding
# CS20
# Mr. Birrell
# Lab 4 Camel Game
import random

# status variables
hydration = 10
hunger = 10
morale = 10
kgb_distance = 20
hours = 0
leave = False
event = False

# intro sequence

print("\nYou were trying to cross the border, right?\nWalked right into that Soviet ambush, same as us, \nand that "
      "thief over there.\n")
name = input("Tell me, what is your name comrade?: ")
print("\nHello comrade", name, ",", "welcome to Siberia. We have been sent to the Gulags."
                                    "\nWe will escape this place before they have a chance to "
                                    "either starve, freeze, or work us to death.")
print("Drinking vodka and eating potatoes gives you 5-10 points each, but your comrades can get hungry or thirsty "
      "too, so you will lose between 1 to 3 points of morale..\n\nSharing you vodka or potatoes with your comrades give"
      " you 1-4 points of hunger or hydration, but it will boost morale by 5-10")

# Game Start
print("Lets go comrade", name, "lets escape this place.")

while not leave:
    if hours >= 24:
        print("You have smuggled yourself to a train where you and your comrades take a train to Poland."
              " You have escaped the KGB and won!\n\n\n\n\n\nfor now....")
        quit()
    elif hydration < 0:
        print("You die from a lack of vodka. Your slavic bloodstream turns to ice and you die.")
        quit()
    elif hunger < 0:
        print("You die from a lack of potato. Your slavic stomach grumbles in agony and you die.")
        quit()
    elif morale < 0:
        print("Your comrades begin to think you are a western spy. in fear of their safety, "
              "they kill you and leave you dead.")
        quit()
    elif kgb_distance < 1:
        print("You got caught by the KGB and is sentenced death. You lose.")
        quit()

    print("What do you suggest we do comrade", name, "?")
    print(hours, "hours have passed.\n")
    print("\nYour hydration is", hydration, "\nYour hunger is", hunger,
          "\nYour comrade's morale is", morale, "\nThe KGB is", kgb_distance, "kilometers away.\n")
    print("A. Drink Vodka and restore hydration\nB. Eat Potato and restore hunger\n"
          "C. Share Vodka with everyone and gain less hydration, but boost morale\n"
          "D. Share Potato with everyone and gain less hunger point, but boost morale\n"
          "E. Run at a moderate speed\nF. Sprint at double speed\nG. Quit the game")

    # Options

    answer = input("\nChoose answer here(a,b,c,d,e,f,g,): ")
    if answer.lower() == "a":
        if event is False:
            event = random.randrange(1, 7)
        elif event == 1:
            print("You found an extra bottle in our lunch box.")
            hydration += random.randrange(1, 4)
        print("You drink the vodka and restore hydration. Your comrades got jealous. Lose morale")
        hydration += random.randrange(5, 11)
        morale += random.randrange(-3, -1)
        kgb_distance += random.randrange(-6, -4)
        hours += 2
        event = False
    elif answer.lower() == "b":
        if event is False:
            event = random.randrange(1, 7)
        elif event == 1:
            print("You found an extra potato in our lunch box.")
            hunger += random.randrange(1, 4)
        print("You ate the potato and your comrades got jealous. Lose morale")
        hunger += random.randrange(5, 11)
        morale += random.randrange(-3, -1)
        kgb_distance += random.randrange(-6, -4)
        hours += 2
        event = False
    elif answer.lower() == "c":
        if event is False:
            event = random.randrange(1, 7)
        elif event == 1:
            print("You and your comrades spot a bunch of Gopniks at the side of the road.\n"
                  "We dance to hardbass, we drink the kvass and help ourselves to snacks.")
            morale += random.randrange(2, 5)
            hunger += random.randrange(2, 5)
            hydration += random.randrange(2, 9)
        event = False
        print("You share vodka with everyone, they restore strength and respect for you, boosting morale.")
        hydration += random.randrange(1, 5)
        morale += random.randrange(5, 11)
        kgb_distance += random.randrange(-6, -4)
        hours += 2
    elif answer.lower() == "d":
        if event is False:
            event = random.randrange(1, 7)
        elif event == 1:
            print("You and your comrades spot a bunch of Gopniks at the side of the road.\n"
                  "We dance to hardbass, we drink the kvass and help ourselves to snacks.")
        print("You share potato with everyone, they restore strength and respect for you, boosting morale.")
        hunger += random.randrange(1, 5)
        morale += random.randrange(5, 11)
        kgb_distance += random.randrange(-6, -4)
        hours += 2
    elif answer.lower() == "e":
        if event is False:
            event = random.randrange(1, 7)
        elif event == 1:
            print("There is a blizzard and it gets in your way. It slows you down slightly.")
            kgb_distance += random.randrange(-2, 1)
        slavic_storm = False
        print("You hiked for 2 hours.")
        kgb_distance += random.randrange(5, 11)
        hydration += random.randrange(-6, -2)
        hunger += random.randrange(-6, -2)
        hours += 2
    elif answer.lower() == "f":
        if event is False:
            event = random.randrange(1, 7)
        elif event == 1:
            print("There is a blizzard and it gets in your way. It slows you down greatly. ")
            kgb_distance += random.randrange(-4, 1)
        else:
            event = False
        print("You traveled as fast as your slavic legs can take you.")
        kgb_distance += random.randrange(15, 21)
        hydration += random.randrange(-16, -12)
        hunger += random.randrange(-16, -12)
        hours += 2
    elif answer.lower() == "g":
        leave = input("are you sure you want to quit? (y / n):")
        if leave.lower() == "y":
            leave = True
            print("Thank you for playing this game comrade", name)
            quit()
    else:
        print("Please pick a character from A-G ")
